package sacabuche.cordova.plugin;

import org.apache.cordova.CordovaPlugin;

import org.apache.cordova.CallbackContext;
import android.content.Intent;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;


public class RequestUpdate extends CordovaPlugin {

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("now")) {
			this.now(args.getString(0), callbackContext);
			return true;
		}
		return false;
	}


	private void now(String path, CallbackContext callbackContext) {
		Log.d("requestUpdate", path);
		if (path != null && path.length() > 0) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(path), "application/vnd.android.package-archive");
			cordova.getActivity().startActivity(intent);
			callbackContext.success("Upgraded");
		} else {
			callbackContext.error("Need a path for the app file");
		}
	} 

}

